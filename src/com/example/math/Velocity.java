package com.example.math;

public class Velocity {

	public static double intersection(Cube A, Cube B){
		
		// check if cubes intersect
		if(A.xMax()<B.xMax() || A.yMax()<B.yMin() || A.zMax()<B.zMin())
			return 0;
		
		// sides of intersected object
		double a=0, b=0, c=0;
		
		// difference in coordinates of two cubes
		double dx, dy, dz;

		dx = A.xMax()-B.xMax();
		dy = A.yMax()-B.yMax();
		dz = A.zMax()-B.zMax();

		/**
		 * Sto se tice objekta koji se dobije kao presek potrebne su nam
		 * njegove stranice a, b i c da bismo izracunali zapreminu.
		 * 
		 * Te stranice mozemo da dobijemo na sledeci nacin (objasnicu za stranicu 'a'
		 * to je stranica paralelna x osi)
		 * 
		 * Ukoliko je razlika izmedju A.xMax i B.xMax veca od nule to znaci da je
		 * A vise desno od B. Pa duzinu stranice 'a' mozemo da izracunamo tako sto
		 * od duzine stranice A oduzmemo razliku izmedju A.xMax i B.xMax (na slici se vidi)
		 * 
		 * Ovaj Math.min() sluzi za situaciju ukoliko je jedna kocka unutar druge.
		 * U tom slucaju ce (A.m - dx) zapravo biti vece od duzine stranice unutrasnje
		 * kocke sto nije moguce i u tom slucaju samo vracamo B.m kao duziunu stranice.
		 */
		if(dx>0)
			a = Math.min(A.m - dx, B.m);
		else
			a = Math.min(B.m + dx, A.m);

		if(dy>0)
			b = Math.min(A.m - dy, B.m);
		else
			b = Math.min(B.m + dy, A.m);

		if(dz>0)
			c = Math.min(A.m - dz, B.m);
		else
			c = Math.min(B.m + dz, A.m);
		
		return a*b*c;
	}
}
