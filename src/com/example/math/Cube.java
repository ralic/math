package com.example.math;

public class Cube {

	public double x;
	public double y;
	public double z;

	public double m;

	public Cube(double x, double y, double z, double m) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.m = m;
	}

	public double xMax(){
		return x + m/2;
	}
	
	public double xMin(){
		return x - m/2;
	}
	
	public double yMax(){
		return y + m/2;
	}
	
	public double yMin(){
		return y - m/2;
	}
	
	public double zMax(){
		return z + m/2;
	}
	
	public double zMin(){
		return z - m/2;
	}
}
